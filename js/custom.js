function HideEverything() {
	$( "div.content_homepage" ).remove();
	$( "div.content_categories" ).remove();
	$( "#left_content" ).remove();
	$( "#right_content" ).remove();
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function setCookie(cname, cvalue) {
	exdays=20*365;
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	}
	return "";
}

function clearCookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function loadJSON(path, success, error)
{
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState === XMLHttpRequest.DONE) {
			if (xhr.status === 200) {
				if (success)
					success(JSON.parse(xhr.responseText));
			} else {
				if (error)
					error(xhr);
			}
		}
	};
	xhr.open("GET", path, true);
	xhr.send();
}

function DoTopic(cat, top, data) {
	var database = data["Database"];
	var topics = data["Topics"];

	$('#topic_sub_title').text(database[top].Name);
	$('#topic_sub_desc').text(database[top].Description);

	var leftHtml = $( "#left_content" ).html();
	
	var htmlString = '';
	for (var i = 0; i < topics[top].length; ++i) {
		var topic = topics[top][i];
		var d_topic = database[topic];

		htmlString += '<div class="row demo-samples" style="width:100%">';
		htmlString += '<div class="todo-search">';
		htmlString += '<p>' + d_topic.Name + '</p>';
		htmlString += '<span>' + d_topic.Description + '</span>';
		htmlString += '</div>';
		htmlString += '<div class="todo"><ul>';

		for (var j = 0; j < d_topic["Content"].length; ++j) {
			var content = database[d_topic["Content"][j]];
			var hasVideo = content.Youtube.length > 0;
			var cookie = "video_" + d_topic["Content"][j] + "_progress";
			var done = getCookie(cookie) == 'yes';
			if (!hasVideo) { done = true; }

			htmlString += '<li';
			if (done) { htmlString += ' class="todo-done"'; }
			htmlString += '><div class="todo-content" onclick="location.href=\'';
			htmlString += 'khan.html?subject=' + cat + '&topic=' + top + '&content=' + d_topic["Content"][j];
			htmlString +='\';"><h4 class="todo-name" style="'
			htmlString += done? 'color:#1abc9c;' : 'color:white;'
			htmlString += '"><i class="fa ';
			htmlString += hasVideo? 'fa-caret-square-o-right' : 'fa-chain-broken';
			htmlString += '"></i>&nbsp;';
			htmlString += content.Name;
			htmlString += '</h4></div></li>';
		}

		htmlString += '</ul></div>';
		htmlString += '</div>';
	}


	$( "#right_replace" ).html(htmlString);
	var rightHtml = $( "#right_content" ).html();

	HideEverything();
	var pstyle = 'border: 1px solid #dfdfdf; padding: 0px;';
	$('#layout').w2layout({
		name: 'layout',
		panels: [
			{ type: 'top', size: 30, style: pstyle, content: '<div id="toolbar" style="padding: 5; border: 0;"></div>' },
			{ type: 'left', size: 400, style: pstyle, content: leftHtml },
			{ type: 'main', style: pstyle, content: rightHtml }
		]
	});

	$('#toolbar').w2toolbar({
		name: 'toolbar',
		items: [
			{ type: 'button',  id: 'item1',  caption: 'Math' },
			{ type: 'break',  id: 'break0' },
			{ type: 'button',  id: 'item2',  caption: database[cat]["Name"] },
			{ type: 'break',  id: 'break1' },
			{ type: 'button',  id: 'item3',  caption: database[top]["Name"] },
		],
		onClick: function (event) {
			if (event.target == 'item1') {
				window.location.href = 'khan.html';
			}
			if (event.target == 'item2') {
				window.location.href = 'khan.html?subject=' + cat;
			}
		}
	});
	w2ui['toolbar'].disable('item3');
}

function DoCategories(cat, data) {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 0px;';
	var content = $( "#categories_content" );
	var categories = data["Categories"][cat];
	var database = data["Database"];
	
	var htmlString = '<h1>' + database[cat].Name + '</h1><p>' + database[cat].Description +'&nbsp;</p><p>&nbsp;</p>';
	htmlString += '<div class="row demo-tiles" style="display:flex;">';
	for (var i = 0; i < categories.length; ++i) {
		var cookie = "category_" + categories[i] + "_progress";
		var active = getCookie(cookie) == 'yes';

		htmlString += '<div class="col-xs-3">';
			htmlString += '<a  class="look_normal" id="' + categories[i] +'" href="khan.html?subject=' + cat + '&topic=' + categories[i] + '">';
				htmlString += '<div class="tile tile'; if (active) { htmlString += ' title-hot' }  htmlString +='" id="ari_title">';
					if (active) htmlString += '<img src="img/ribbon-2x.png" alt="ribbon" class="tile-hot-ribbon">';
					htmlString += '<img src="img/icons/' + categories[i] +'.png" alt="Compas" class="tile-image big-illustration">'; 
					htmlString += '<h3 class="tile-title subject_title">' + database[categories[i]].Name +'</h3>';
				htmlString += '</div>';
			htmlString += '</a>';
		htmlString += '</div>';
		if ((i + 1) % 4 == 0 && i != categories.length - 1) {
			htmlString += '</div>';
			htmlString += '<div class="row demo-tiles" style="display:flex;">'
		}
	}
	htmlString += '</div>';

	content.append(htmlString);
	var mainContent = $( "#categories_container" ).html();
	HideEverything();

	$('#layout').w2layout({
		name: 'layout',
		panels: [
			{ type: 'top', size: 30, style: pstyle, content: '<div id="toolbar" style="padding: 5; border: 0;"></div>' },
			{ type: 'main', style: pstyle, content: mainContent }
		]
	});

	$('#toolbar').w2toolbar({
		name: 'toolbar',
		items: [
			{ type: 'button',  id: 'item1',  caption: 'Math' },
			{ type: 'break',  id: 'break0' },
			{ type: 'button',  id: 'item2',  caption: data["Database"][cat]["Name"] },
		],
		onClick: function (event) {
			if (event.target == 'item1') {
				window.location.href = 'khan.html';
			}
		}
	});
	w2ui['toolbar'].disable('item2');
}

function DoHomepage(data) {
	var pstyle = 'border: 1px solid #dfdfdf; padding: 0px;';
	var mainContent = $( "div.content_homepage" ).html();
	HideEverything();

	$('#layout').w2layout({
		name: 'layout',
		panels: [
			{ type: 'top', size: 30, style: pstyle, content: '<div id="toolbar" style="padding: 5; border: 0;"></div>' },
			{ type: 'main', style: pstyle, content: mainContent }
		]
	});

	$('#toolbar').w2toolbar({
		name: 'toolbar',
		items: [
			{ type: 'button',  id: 'item1',  caption: 'Math' },
		],
		onClick: function (event) { }
	});
	w2ui['toolbar'].disable('item1');

	for (var i = 0; i < data.Subject.length; ++i) {
		var subject = data["Subject"][i];
		var cookie = 'subject_'+subject+'_progress';

		if (getCookie(cookie) == 'yes') {
			var selectorId = '#'+subject+'_title';
			$( selectorId ).addClass( "tile-hot" );
			$( selectorId ).prepend( '<img src="img/ribbon-2x.png" alt="ribbon" class="tile-hot-ribbon">');
		}
	}
}